﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ej3 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        int n = 73;
        if (isPrime(n))
        {
            Debug.Log("Es primo");
        }
        else {
            Debug.Log("No es primo");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    bool isPrime(int n) {

        if (n == 1) return true;
        if (n == 2) return true;

        if (n % 2 == 0) return false; // Even number     

        for (int i = 2; i < n; i++)
        { // Advance from two to include correct calculation for '4'
            if (n % i == 0) return false;
        }

        return true;


    }
}
