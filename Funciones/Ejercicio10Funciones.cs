﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ejercicio10Funciones : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //numero enemigos
        int nEnemigos = Random.Range(1, 6);
        //Creamos la lista de enemigos.
        List<int[]> enemigos = new List<int[]>();
        //el array con las posiciones del jugador.
        int[] posicionesJugador = { 0, 0 };   
        //Añadimos 6 enemigos para probar en posiciones aleatorias
        for (int i=0; i<nEnemigos; i++) {         
            enemigos.Add(new int[] { Random.Range(-100, 120), Random.Range(-100, 120) });
        }          
       Debug.Log("El enemigo más cercano es el: " + getClosestEnemyIndex(posicionesJugador,enemigos));
    }

    // Update is called once per frame
    void Update()
    {       
    }


    //Funcion para calcular el indice del enemigo más cercano 
    int getClosestEnemyIndex(int[] posicionJugador, List<int[]>enemyPositions) {
        int positionEnemy=0;
        float actualDistance =0;
        float minumDistance=999999999999999999;
       //Recorremos la lista de enemigos para calcular la distancia que hay entre el jugador y TODOS los enemigos.
        for (int i=0; i<enemyPositions.Count; i++) {
            //Actualizamos la distancia actual.       
           actualDistance = calculateEucleanDistance(posicionJugador, enemyPositions[i]);
           /*Si el valor positivo de esta (Distancia actual) es mayor que la máxima distancia la machacamos 
            y obtenemos el indice(Posición actual del bucle, por ende posición del array).*/
            if (Mathf.Abs(actualDistance) < Mathf.Abs(minumDistance)) {
                minumDistance = actualDistance;
                positionEnemy = i;             
            } 
        }
        return positionEnemy;
    }
    //Formula distancia Euclidea
    float  calculateEucleanDistance(int [] jugador, int [] enemigo) {
        //Fórmula de distancia euclidea
        /*
         * La raíz de la suma de las potencias de la resta entre dos puntos X e Y.
         */
             return Mathf.Sqrt(Mathf.Pow((enemigo[0] - jugador[0]), 2) + Mathf.Pow((enemigo[1] - jugador[1]), 2));
    }
}
