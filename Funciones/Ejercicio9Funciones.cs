﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Ejercicio9Funciones : MonoBehaviour
{
    public string race;
    // Start is called before the first frame update
    void Start()
    {        
        int [] valoresPersonaje = createRPGCharacter(race);

        if (!String.IsNullOrEmpty(race))
        {
            Debug.Log("Your Character " +
                convertToEnglish(race) + ": has " 
                + valoresPersonaje[0] + " STR, "
                + valoresPersonaje[1] + " SPD, " 
                + valoresPersonaje[2] + "VIT, " 
                + valoresPersonaje[3] + " DEX, " 
                + valoresPersonaje[4] + " MAG.");
        }
        else
        {
            Debug.Log("Please insert a character... ");
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    int[] createRPGCharacter(string race)
    {       
            int str = 0;
        int spd = 0;
        int vit = 0;
        int dex = 0;
        int mag = 0;
        switch (convertToEnglish(race))
        {
            case "human":
                str = 8 + diceThrow(6) + diceThrow(6);
                spd = 4 + diceThrow(6);
                vit = diceThrow(20);
                dex = diceThrow(20);
                mag = 1 + diceThrow(10);
                break;
            case "elven":
                str = 6 + diceThrow(4)+ diceThrow(4);
                str = 6 + diceThrow(4)+ diceThrow(4);
                vit = diceThrow(20);
                dex = 3+diceThrow(10)+diceThrow(10);
                mag = diceThrow(20);
                break;
            case "dwarf":
                str = 6 + diceThrow(10);
                spd = diceThrow(8)+diceThrow(8);
                vit = 10 + diceThrow(8);
                dex =  diceThrow(20);
                mag = diceThrow(8);
                break;
            case "mage":
                str = diceThrow(6);
                spd = diceThrow(20);
                vit = diceThrow(10);
                dex = diceThrow(20);
                mag = 12 + diceThrow(8);
               break;
            default:
               str = 0;
                spd = 0;
                vit = 0;
                dex = 0;
                mag = 0;
                Debug.Log("This race does not exist, try: Human, Elven, Dwarf, Mage");break;
        }
      
    
   
        return new int[] {str,spd,vit,dex,mag};
    }

    int diceThrow(int nCaras)
    {
        int caras = Random.Range(1, (nCaras+1));
        return caras;
    }
          
    string stringRaceToLower(string race) {

        return race.Replace(" ", "").ToLower();
    }
    
    string convertToEnglish(string race) {
        string raceInEnglish = "";
        if (stringRaceToLower(race).Equals("humano") || stringRaceToLower(race).Equals("humana"))
        {
            raceInEnglish = "human";
        }
        else if (stringRaceToLower(race).Equals("elfo") || stringRaceToLower(race).Equals("elfa"))
        {
            raceInEnglish = "elven";
        }
        else if (stringRaceToLower(race).Equals("enano") || stringRaceToLower(race).Equals("enana"))
        {
            raceInEnglish = "dwarf";
        }
        else if (stringRaceToLower(race).Equals("mago") || stringRaceToLower(race).Equals("maga"))
        {
            raceInEnglish = "mage";
        }
        else {
            raceInEnglish = race;
        }
        return raceInEnglish;

    }


}
