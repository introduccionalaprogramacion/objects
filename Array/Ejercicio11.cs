﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ejercicio11 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        List<string> baraja = crearListaCartas();
        int puntuacion=0;
        int valorAs;
        string carta = "";

        for (int i = 0; i < 5; i++)
        {
            int nCarta = Random.Range(0, baraja.Count);
            carta = baraja[nCarta];
            baraja.Remove(carta);
            string numCrta = carta.Substring(0, 2).ToLower().Replace(" ", "");
            switch (numCrta)
            {
                case "as":
                    valorAs = Random.Range(1, 3);
                    if (valorAs == 1)
                    {
                        puntuacion = 1;
                    }
                    else
                    {
                        puntuacion = 11;
                    }; break;
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                case "8":
                case "9": puntuacion = int.Parse(numCrta); break;
                case "10":
                case "j":
                case "q":
                case "k": puntuacion = 10; break;



            }
            Debug.Log("La carta "+carta+" otorga una puntuación de: "+puntuacion);


        }
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public List<string> crearListaCartas()
    {
        List<string> cartas = new List<string>();
        cartas.Add("As de espadas");
        for (int j = 2; j <= 12; j++)
        {
            if (j <= 10)
            {
                cartas.Add(j + " de espadas.");
            }
            else
            {
                switch (j)
                {
                    case 11: cartas.Add("J de espadas."); break;
                    case 12: cartas.Add("Q de espadas."); break;
                    case 13: cartas.Add("K de espadas."); break;
                }

            }

        }
        cartas.Add("As de corazones");
        for (int j = 2; j <= 12; j++)
        {
            if (j <= 10)
            {
                cartas.Add(j + " de corazones.");
            }
            else
            {
                switch (j)
                {
                    case 11: cartas.Add("J de corazones."); break;
                    case 12: cartas.Add("Q de corazones."); break;
                    case 13: cartas.Add("K de corazones."); break;
                }

            }

        }
        cartas.Add("As de tréboles");
        for (int j = 2; j <= 12; j++)
        {
            if (j <= 10)
            {
                cartas.Add(j + " de tréboles.");
            }
            else
            {
                switch (j)
                {
                    case 11: cartas.Add("J de tréboles."); break;
                    case 12: cartas.Add("Q de tréboles."); break;
                    case 13: cartas.Add("K de tréboles."); break;
                }

            }

        }
        cartas.Add("As de diamantes");
        for (int j = 2; j <= 12; j++)
        {
            if (j <= 10)
            {
                cartas.Add(j + " de diamantes.");
            }
            else
            {
                switch (j)
                {
                    case 11: cartas.Add("J de diamantes."); break;
                    case 12: cartas.Add("Q de diamantes."); break;
                    case 13: cartas.Add("K de diamantes."); break;
                }

            }

        }
        return cartas;
    }
}
