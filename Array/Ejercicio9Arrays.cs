﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ejercicio9Arrays : MonoBehaviour
{
    
    // Start is called before the first frame update
    void Start()
    {
        //0-13, 13- 25, 26-38, 39-48
        List<string> cartasJuego = crearListaCartas();  
        int cartasARepartir = Random.Range(1, 6);
        Debug.Log("------CARTAS DEL JUGADOR----------");
        for (int i = 0; i <cartasARepartir; i++)
        {
            string carta = cartasJuego[Random.Range(0, cartasJuego.Count)];
            Debug.Log(carta);            
            cartasJuego.Remove(carta);
        }
        
       
       

    }

    // Update is called once per frame
    void Update()
    {

    }

public List<string> crearListaCartas()
    {
        List<string> cartas = new List<string>();
        cartas.Add("As de espadas");
        for (int j = 2; j <= 12; j++)
        {
            if (j<=10) {
                cartas.Add(j + " de espadas.");
            }
            else
            {
                switch (j) {
                    case 11: cartas.Add("J de espadas.");break;
                    case 12: cartas.Add("Q de espadas.");break;
                    case 13: cartas.Add("K de espadas.");break;
                }

            }
           
        }
        cartas.Add("As de corazones");
        for (int j = 2; j <= 12; j++)
        {
            if (j <= 10)
            {
                cartas.Add(j + " de corazones.");
            }
            else
            {
                switch (j)
                {
                    case 11: cartas.Add("J de corazones."); break;
                    case 12: cartas.Add("Q de corazones."); break;
                    case 13: cartas.Add("K de corazones."); break;
                }

            }

        }
        cartas.Add("As de tréboles");
        for (int j = 2; j <= 12; j++)
        {
            if (j <= 10)
            {
                cartas.Add(j + " de tréboles.");
            }
            else
            {
                switch (j)
                {
                    case 11: cartas.Add("J de tréboles."); break;
                    case 12: cartas.Add("Q de tréboles."); break;
                    case 13: cartas.Add("K de tréboles."); break;
                }

            }

        }
        cartas.Add("As de diamantes");
        for (int j = 2; j <= 12; j++)
        {
            if (j <= 10)
            {
                cartas.Add(j + " de diamantes.");
            }
            else
            {
                switch (j)
                {
                    case 11: cartas.Add("J de diamantes."); break;
                    case 12: cartas.Add("Q de diamantes."); break;
                    case 13: cartas.Add("K de diamantes."); break;
                }

            }

        }
        return cartas;
    }
}
