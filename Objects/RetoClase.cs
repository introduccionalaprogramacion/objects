﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RetoClase : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Enemigo[] enemigos = 
            { new Enemigo("Goblin", 4, 4, 120),
            new Enemigo("Slime", 1, 1, 20),
            new Enemigo("Fainalu Bossu", 1250, 1256, 456184878),
            new Enemigo("Lobo veterano", 4, 10, 168),
            new Enemigo("Cubo To Tocho", 1100, 1350, 4561848)};

        for (int i=0; i<enemigos.Length; i++) {
            Debug.Log(enemigos[i].ToString());

        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
