using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player{
    public List<Card> cartasJugador= new List<Card>();
    public int puntuacion;

  public List<Card> dealCard(Card carta) {
        this.cartasJugador.Add(carta);
        setPoints(carta);
        return this.cartasJugador;
    }
  private void setPoints(Card carta) {
       
        this.puntuacion+=carta.puntos;   
         }

  public int getPoints() {
        return this.puntuacion;
    }
}
