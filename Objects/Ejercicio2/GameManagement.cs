﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagement : MonoBehaviour {
    // Start is called before the first frame update
    void Start()
    {
        List<Card> cartasJuego = initDeck();       
        Player player1 = new Player();


        List<Card> cartasJugador = player1.dealCard(nextCard(cartasJuego));
        Debug.Log("Has obtenido: "+player1.getPoints()+" puntos.");
        Debug.Log("Con las siguientes cartas: ");
        try {
            for (int i = 0; i < cartasJugador.Count; i++) {
               cartasJugador[i].printInfo();
            }
        }
        catch (System.IndexOutOfRangeException oute) {
            Debug.Log(oute, this);
        }
        catch (System.NullReferenceException nre) {
            Debug.Log(nre, this);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    List<Card> initDeck()
    {
        List<Card> cartas = new List<Card>();
        List<Card> cartaBarajada = new List<Card>();
        cartas.Add(new Card("Espadas", "As", 10));
        for (int i = 2; i <= 13; i++)
        {
            if (i >= 10)
            {
                cartas.Add(new Card("Espadas", i.ToString(), 10));
            }
            else
            {
                cartas.Add(new Card("Espadas", i.ToString(), i));
            }
        }
        cartas.Add(new Card("Rombos", "As", 10));
        for (int i = 2; i <= 13; i++)
        {
            if (i >= 10)
            {
                cartas.Add(new Card("Rombos", i.ToString(), 10));
            }
            else
            {
                cartas.Add(new Card("Rombos", i.ToString(), i));
            }
        }
        cartas.Add(new Card("Corazones", "As", 10));
        for (int i = 2; i <= 13; i++)
        {
            if (i >= 10)
            {
                cartas.Add(new Card("Corazones", i.ToString(), 10));
            }
            else
            {
                cartas.Add(new Card("Corazones", i.ToString(), i));
            }
        }
        cartas.Add(new Card("Tréboles", "As", 10));
        for (int i = 2; i <= 13; i++)
        {
            if (i >= 10)
            {
                cartas.Add(new Card("Tréboles", i.ToString(), 10));
            }
            else
            {
                cartas.Add(new Card("Tréboles", i.ToString(), i));
            }
        }
        for (int i = cartas.Count; i >= 0; i--)
        {
            int nRandom = Random.Range(0, cartas.Count);
            Card newCard = cartas[nRandom];
            cartaBarajada.Add(newCard);
            cartas.Remove(newCard);
        }
        return cartaBarajada;

    }
    Card nextCard(List<Card>cartasRepartir) {
        try
        {
            Card cartaRepartir = cartasRepartir[Random.Range(0, cartasRepartir.Count)];
            cartasRepartir.Remove(cartaRepartir);
            return cartaRepartir;
        }
        catch (System.IndexOutOfRangeException oute) {
            Debug.Log(oute+". Something went wrong, try again", this);
            return null;

        }
     }


}
