﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerTetromino : MonoBehaviour
{      
    string[] type = {"straight","square", "L-tetromino", "T-tetromino", "Z-tetromino" };
    string[] color = {"rojo","azul","verde","rosa", "amarillo" };
    List<Tetromino> tetris = new List<Tetromino>();

    // Start is called before the first frame update
    void Start()
    {
        for (int i=0; i<10; i++) { 
        tetris.Add(newPieza());
        }
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    Tetromino newPieza() {       
        return new Tetromino(type[Random.Range(0, type.Length)],color[Random.Range(0, type.Length)]);
    }
}
