﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ejercicio12 : MonoBehaviour
{
    public int distanciaEntreEnemigos;
    public int numeroEnemigos;
    public int posicionX = 0;
    int posicionProximaEnemigo;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < numeroEnemigos; i++)
        {
            posicionProximaEnemigo += distanciaEntreEnemigos + posicionX;
            Debug.Log("El próximo enemigo se encuentra en la posición: " + posicionProximaEnemigo + " y en: " + (posicionProximaEnemigo * -1));
        }
    
        for (int i = 0; i < numeroEnemigos; i++)
        {
           posicionX = distanciaEntreEnemigos + posicionX;
            Debug.Log("El próximo enemigo se encuentra en la posición: " + posicionX + " y en: " + (posicionX * -1));
        }

    }

    // Update is called once per frame
    void Update()
    {

    }
}
