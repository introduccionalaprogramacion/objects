﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ejercicio9 : MonoBehaviour
{
    public int n;
 
    // Start is called before the first frame update
    void Start()
    {
        int contador3 = 0;
        int contador5 = 0;
        int contador15 = 0;
        for (int i=0; i<n;i--) {
            if (n%3==0) {
                contador3++;
            }
            if (n%5==0) {
                contador5++;
            }
            if (n%3==0&&n%5==0) {
                contador15++;
            }
        }

        Debug.Log("Hay "+contador3+" múltiplos de 3 " + contador5 + " múltiplos de 5 y "+contador15 + " múltiplos de 3 y 5 a la vez.");
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
