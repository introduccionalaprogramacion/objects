﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ejercicio8 : MonoBehaviour
{
    public int valor1;
    public int valor2;
    bool isMultiplo;

    // Start is called before the first frame update
    void Start()
    {
        while (!isMultiplo) {
            valor2++;
            if ((valor2 % valor1) != 0)
            {
                Debug.Log("No es multiplo");
            }
            else {
                Debug.Log("El multiplo más cercano es: "+valor2);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
