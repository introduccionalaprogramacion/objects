﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ejercicio3 : MonoBehaviour
{
    public int valor;
    int suma = 0;
    int resto;
    // Start is called before the first frame update
    void Start()
    {
        if (valor >= 0)
        {
            for (int i = 0; i <= valor; i++)
            {
                resto = valor / 2;
                if (resto != 0)
                {
                    suma += i;
                }
            }
            Debug.Log(suma);
        }
        else {
            Debug.Log("El numero no es positivo.");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
